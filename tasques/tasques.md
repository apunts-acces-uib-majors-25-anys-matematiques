# Versions 


## 2.8 

### Notes 

- [#55] Versió per imprimir. Llibre a tenda online (lulu, amazon, etc.)

### Tasks 

- [#16] Posar les solucions (que no la resolució) 
- [#22] Fer dibuixos de com són les rectes i plans relatius 
- [#35] Fer exercicis i exemples de producte escalar, vectorial, mixt de geometria 3D; així com exercicis de trobar el volum de paral·lelepíped i tetraedre 
- [#37] Posar exemple de posició relativa entre dues rectes a l'espai 
- [#38] Posar exemple de posició relativa entre dos plans 
- [#76] Fer exàmens de prova: incloure els altres blocs 
- [#79] Repassar els accents diacrítics i acomodar-los a la nova normativa 
- [#81] marges: asimètrics? 
- [#85] L'exemple de 'Pas entre equacions d'una recta' hauria de ser de pas d'una equació a una altra 
- [#87] Llevar la secció 'Exercicis resolts de geometria de l'espai' 

## 2.9 

### Notes 

- [#28] Versió pel curs posterior al de la versió @2.7, si passa

### Tasks 

- [#8] Crear més exercicis per aplicar propietats de probabilitat (inclosa la propietat condicionada): mirar de fer més exercicis tipus el 299, 300, 301, 304 i 306 de la versió 2.5.0 [https://repo.or.cz/apunts-acces-uib-majors-25-anys-matematiques.git/blob/refs/tags/v2.5.0:/apunts.pdf] 
- [#13] Canviar gràfic png del paral·lelepípede a un vectorial usant TikZ (definició 56: un paral·lelepípede és un prisme de la base del qual...) [https://latex.net/tikz-3dplot/] [https://tex.stackexchange.com/questions/654533/drawing-a-parallelopiped-in-tikz] [https://tex.stackexchange.com/questions/512415/are-there-simple-ways-to-draw-parallelepipeds-in-tikz] 
- [#20] Els exercicis (i les solucions) poden estar després de la secció corresponent?. En comptes del darrera, hi podria haver exercicis en acabar una secció 
- [#34] La resolució de l'exercici 108b (segons la resolució) (42b segons la versió 2.7) està malament: la darrera columna de M està malament: toca ser 0, 0, 2 i no 3, 1, 4. A més, no està acabat 
- [#40] Incorporar els apunts de la UIB antics [/home/xan/zync/antics/SYNC/antic-cepasud-raw/classe/Accés UIB/] 
- [#83] bibliography amb biblatex? 
- [#84] biblatex personalitzat: https://www.khirevich.com/latex/bibliography/ 
- [#86] Per què necessit el teorema de Bayes? Posar un exemple concret d'aplicació 

## 3.0 

### Notes 

- [#2] Manual autocontingut, és a dir, que no faci falta saber res defora del manual

### Tasks 

- [#11] Posar enllaços a recursos: podríem posar una nova al marge amb els recursos externs del tema que tractassim (probablement els meus vídeos explicant la teoria o algun exercicis) 
- [#15] Mecanografiar la resolució dels exercicis (tenc manuscrita una part) 
- [#17] Reduir el manual a 100 pàgines màxim 
- [#18] Re-escriure l'estil: més directa, no tant acadèmic. Ha de ser més directa: teoria essencial (com es fa una cosa) i exercicis d'allò. Com a fitxes independents unes de les altres 

## Algun-dia 

### Notes 

- [#7] Canvis que impliquin *noves* millores, no de revisió. Coses que he de fer sense un termini definit. Algun dia

### Tasks 

- [#14] Escriure la part d'anàlisi: sobretot perquè potser el bloc d'anàlisi és més mecànic que el de geometria. Molts alumnes no veuen la geometria. Potser el bloc és més curt que el de geometria. Només hauria d'escriure el que surt als exàmens, estrictament. Així cada any podria anar triant. Necessitaria potser passar de 11pt a 10pt. 
- [#21] Fixar els warnings que surten quan compilam 
- [#42] Posar els exercici dels anys anteriors al 2010 com a exercicis! 
- [#52] Fer una versió HTML amb BookML [https://vlmantova.github.io/bookml/] (és el que fa servir openlogicproject) 
- [#73] Escriure la part d'Estadística que formalment forma part del temari, encara que fins 2024 mai ha sortit als exàmens 

