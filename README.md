<!--
SPDX-FileCopyrightText: 2023 Xavier Bordoy

SPDX-License-Identifier: CC-BY-4.0
-->

# Apunts de Matemàtiques per a l'accés a la UIB per a majors de 25 anys

## Continguts

En aquest repositori trobareu el codi font dels *Apunts per a l'accés a la UIB per a majors de 25 anys*. L'objectiu d'aquests apunts és servir de guia per a aprovar la prova de Matemàtiques de l'[Accés a la Universitat de les Illes Balears (UIB) per a majors de 25 anys](https://estudis.uib.es/estudis-de-grau/Com-hi-pots-accedir/acces/mes_grans25/).

Si voleu la darrera versió dels apunts, teniu diverses opcions:

- [descarregar el fitxer](apunts.pdf).
- comanar una [còpia impresa](https://www.lulu.com/shop/xisco-sebasti%C3%A0-and-xavier-bordoy/apunts-de-matem%C3%A0tiques-per-a-lacc%C3%A9s-a-la-uib-per-a-majors-de-25-anys/paperback/product-5792yw7.html?page=1&pageSize=4).

Nota: encara que s'intenta que la còpia impresa sigui exactament igual que la versió digital, he d'advertir que a estones està desfasada, ja que l'actualització s'ha de fer a mà. Per tant, si voleu la darrera versió dels apunts, heu de consultar la versió digital.

## Autoria

L'autoria d'aquesta obra és compartida entre tots els col·laboradors d'aquest projecte: Xisco Sebastià, Xavier Bordoy i qualsevol persona que, eventualment, hi participi.

Qualsevol persona que contribueix al projecte està d'acord tàcitament amb la llicència emprada en l'obra.

El manteniment d'aquest repositori corre a càrrec de mi mateix, Xavier Bordoy.

# Drets d'autor #

Els continguts d'aquest repositori estan subjectes a la llicència de [Reconeixement de Creative Commons 4.0](http://creativecommons.org/licenses/by/4.0/deed.ca) (CC-BY 4.0), llevat que s'hi indiqui el contrari. Això vol dir, *essencialment*, que podeu copiar, modificar i redistribuir l'obra sempre que en citeu de manera adequada la font i si hi heu fet algun canvi (vegeu el directori `LICENSES/`).

Com a garantia i transperència, el repositori ha passat la prova de [`reuse lint`](https://reuse.readthedocs.io).

## Retroacció i suport

Si feis servir aquest manual, si us plau digueu-m'ho a través del correu electrònic, que trobareu a l'esmentat document PDF. No el faig públic aquí per evitar problemes d'*spam*. També ho podeu fer si trobeu alguna cosa que es pugui millorar (segur que en trobeu alguna!). Això m'alegrarà molt.

A part del suport moral comentat anterioment, també me'n podeu donar algun de més material: podeu donar-me [alguna propina](https://www.paypal.com/donate/?business=Y47CYDYMBBVX4&no_recurring=0&item_name=creating+new+math+activities%2C+software%2C+exercises+and+reflections&currency_code=EUR) de forma simbòlica.

## Coses a fer

Teniu la llista de [tasques a fer](tasques/tasques.md) i la [llista de tasques fetes](tasques/.taskbook/archive/archive.json) per si li voleu pegar una ullada. Us el programari [taskbook](https://github.com/klaussinani/taskbook) per administrar les tasques.

La versió del document és la que figura a [aquest fitxer](versio.txt). De forma general, seguiré l'esquema de versions recollit als documents ["versions semàntiques per a documents"](https://semverdoc.org/) i ["versió semàntiques per a programari"](https://semver.org/): `Versió.Subversio.revisió`: de forma habitual, els canvis a la versió corresponen a canvis abruptes, els de la subversió a millores i els de la revisió a petites correccions.

