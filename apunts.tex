% SPDX-FileCopyrightText: 2023 Xisco Sebastià, Xavier Bordoy
%
% SPDX-License-Identifier: CC-BY-4.0

% Tipus i mida del document
\documentclass[11pt]{book}
%% US Executive 7x10in
\usepackage[paperwidth=7in, paperheight=10in, hmargin=2cm, vmargin=2cm, twoside]{geometry}
% Language
\usepackage{polyglossia}
\setdefaultlanguage{catalan}
% Links
\usepackage{nameref}
\usepackage{xurl}
%% Only boxes, not colours
\usepackage[colorlinks=false,
            pdfauthor={Xavier Bordoy, Xisco Sebastià},
            pdftitle={Apunts de Matemàtiques per a l'accés a la UIB per a majors de 25 anys},
            pdfsubject={Mathematics, Matemàtiques},
	    pdfmoddate={\today},
            pdfkeywords={UIB, majors de 25 anys, accés, provés, examen, preparació, adults, àlgebra lineal, determinants, matrius, sistemes d'equacions, geometria cartesiana, vectors, rectes, plans, probabilitat, PAU},
            catalan]{hyperref}

\usepackage{bookmark}
% Euro
\usepackage{eurosym}
% Colour
\usepackage[usenames,dvipsnames,svgnames,table]{xcolor}
% Headers
\usepackage{fancyhdr}
\usepackage{titlesec}
% Graphics
\usepackage{graphicx}
% AMS packages
\usepackage[reqno]{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{gensymb}
% Font
\usepackage{kpfonts}
% Theorems packages
\usepackage{amsthm}
\usepackage{thmtools}
% Units
\usepackage{siunitx}
% Tables
\usepackage{array}
% Glossari
\usepackage{imakeidx}
% Enumerations easyly
\usepackage[inline]{enumitem}
% Multicols
\usepackage{multicol}
% Framed
\usepackage{framed}
% TiKZ i PGF
\usepackage{tikz}
\usepackage{pgfmath}
\usepackage{mathrsfs}
\usetikzlibrary{decorations.markings, mindmap,calc,intersections, through, backgrounds, arrows, shapes.geometric, fadings, decorations.pathreplacing, shadings, positioning, shapes, matrix, snakes, matrix,patterns, trees}
\usepgflibrary{shapes.geometric,intersections}

\usepackage{tikz-3dplot} 

% Captions
\usepackage{caption}
\usepackage{subcaption}

% Theorems (thmtools options)
% Porting amsthm definitions https://github.com/somenxavier/apunts-acces-uib-per-a-majors-de-25-anys/commit/24cc42cf1ed88a1596434012ac943d0995cec56f
% refname i Refname for using \autoref, \nameref, \cref i \Cref (see thmtools documentation)

%% Theorems' styles
\declaretheoremstyle[
headfont=\normalfont\itshape,
bodyfont=\normalfont,
qed=\blacksquare
]{tipusdemo}

\declaretheoremstyle[
headfont=\normalfont\sffamily\scshape\bfseries,
bodyfont=\normalfont\itshape
]{tipusthm}

\declaretheoremstyle[
headfont=\normalfont\sffamily\scshape\bfseries,
bodyfont=\normalfont
]{tipusdef}

\declaretheoremstyle[
headfont=\normalfont\sffamily\scshape,
bodyfont=\normalfont
]{tipusexemple}


%% Results, algorithm, and so
\declaretheorem[name=Teorema, refname={teorema,teoremes}, Refname={Teorema, Teoremes},style=tipusthm]{theorem}
\declaretheorem[name=Coro\lgem{ari}, sibling=theorem, refname={coro\lgem{ari},coro\lgem{aris}}, Refname={Coro\lgem{ari}, Coro\lgem{aris}},style=tipusthm]{corollary}
\declaretheorem[name=Lema, sibling=theorem, refname={lema, lemes}, Refname={Lema, Lemes},style=tipusthm]{lemma}
\declaretheorem[name=Proposició,sibling=theorem, refname={proposició,proposicions}, Refname={Proposició, Proposicions},style=tipusthm]{proposition}
\declaretheorem[name=Algorisme, refname={algorisme,algorismes}, Refname={Algorisme, Algorismes},style=tipusthm]{algorithm}
\declaretheorem[name=Condició, refname={condició,condicions}, Refname={Condició, Condicions},style=tipusthm]{condition}

%% Definitions, etc.
\declaretheorem[style=tipusdef, name=Definició, refname={definició,definicions}, Refname={Definició, Definicions}]{definition}
\declaretheorem[style=tipusdef, name=Notació, refname={notació,notacions}, Refname={Notació,Notacions}]{notation}

%% Examples and claims
\declaretheorem[style=tipusexemple, name=Exemple, refname={exemple, exemples}, Refname={Exemple, Exemples}]{example}
\declaretheorem[style=tipusexemple, name=Observació, refname={observació,observacions}, Refname={Observació, Observacions},style=tipusdef]{claim}

%% Notes, exercises, etc.
%%% era style=remark
\declaretheorem[style=tipusexemple,name=Nota, refname={nota,notes}, Refname={Nota, Notes}]{remark}
\declaretheorem[style=tipusexemple,name=Exercici, refname={exercici, exercicis}, Refname={Exercici, Exercicis}]{exercise}

%% Solutions and resolutions
\declaretheorem[style=tipusdemo,name=Solució, refname={solució,solucions}, Refname={Solució, Solucions}]{solution}
%% Demonstrations
\declaretheorem[style=tipusdemo,name=Demostració, refname={demostració,demostracions}, Refname={Demostració, Demostracions},numbered=no]{demonstration}
%%% For solution without enumeration
\declaretheorem[style=tipusdemo,name=Solució, refname={solució,solucions}, Refname={Solució, Solucions},numbered=no]{solution*}
\declaretheorem[style=tipusexemple,name=Solució, refname={solució,solucions}, Refname={Solució, Solucions},numbered=no]{solutions-final}
\declaretheorem[style=tipusdemo,name=Resolució, refname={resolució,resolucions}, Refname={Resolució, Resolucions},numbered=no]{resolution}


%% macros

\newcommand{\term}[1]{{\bf\em #1}}

% Page headings styles (fancyhdr options)
\setlength{\headheight}{15pt} 
\pagestyle{fancy}

\fancyhf{}

\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\footrulewidth}{0pt}
\fancyhead[LO]{\textit{\nouppercase{\leftmark}}}
\fancyhead[RE]{\textit{\nouppercase{\rightmark}}}
\fancyhead[RO,LE]{\thepage}

% Headers options (titlesec)
\titleformat
{\chapter} % command
[hang] % shape
{\huge} % format itshape
{\bfseries\LARGE\thechapter} % label
{1ex} % sep
{
    \centering~\\[1ex]
    \bfseries%
} % before-code
[
\vspace{3cm}
] % after-code

% Section, subsections, etc. formatting
\titleformat{\section}
  {\bfseries\Large\selectfont}{\thesection}{1em}{}

\titleformat{\subsection}
  {\bfseries\large\selectfont}{\thesubsection}{1em}{}

\titleformat{\subsubsection}
  {\bfseries\selectfont}{\thesubsubsection}{1em}{}

% Allow page breaks in equations
\allowdisplaybreaks

% Glossari
\makeindex[intoc, title=Glossari]

% Setting title of List of theorems
\renewcommand\listtheoremname{Índex de resultats}

% Commands of TiKz for planes
\input{tikz-comandes}

% Increase the verbosity of TOC until subsubsections
\setcounter{tocdepth}{3}
\setcounter{secnumdepth}{3}

% Manual hyphenations

\begin{hyphenrules}{catalan}
\hyphenation{ac-ci-dent}
\hyphenation{a-con-se-guir}
\hyphenation{a-li-ment}
\hyphenation{am-du-es}
\hyphenation{a-na-lí-ti-ca-ment}
\hyphenation{an-te-rior}
\hyphenation{a-ques-ta}
\hyphenation{a-quests}
\hyphenation{ar-rels}
\hyphenation{a-tra-ves-sar}
\hyphenation{au-to-mà-ti-ca-ment}
\hyphenation{au-to-mò-bils}
\hyphenation{bi-di-men-sio-nal}
\hyphenation{bo-lles}
\hyphenation{co-e-fi-ci-ent}
\hyphenation{co-e-fi-ci-ents}
\hyphenation{co-in-ci-dents}
\hyphenation{co-lum-na}
\hyphenation{co-lum-nes}
\hyphenation{com-pleix}
\hyphenation{com-po-nent}
\hyphenation{com-po-si-ci-ó}
\hyphenation{Con-si-de-rem}
\hyphenation{con-su-mei-xen}
\hyphenation{con-tin-gèn-ci-a}
\hyphenation{cor-res-po-nent}
\hyphenation{Cre-a-tive}
\hyphenation{de-ma-na}
\hyphenation{de-no-mi-na-dors}
\hyphenation{de-pen-dents}
\hyphenation{des-com-pon-dre}
\hyphenation{des-comp-tes}
\hyphenation{des-co-ne-gu-da}
\hyphenation{de-ter-mi-nant}
\hyphenation{de-ter-mi-nat}
\hyphenation{De-ter-mi-neu}
\hyphenation{de-ter-mi-nis-ta}
\hyphenation{di-ag-nos-ti-car}
\hyphenation{di-fe-rent}
\hyphenation{di-fe-rents}
\hyphenation{dis-po-sen}
\hyphenation{dis-tri-bu-ció}
\hyphenation{dis-tri-bu-eix}
\hyphenation{diu-men-ge}
\hyphenation{e-le-men-tal}
\hyphenation{em-pre-sa-rials}
\hyphenation{en-tre}
\hyphenation{e-qua-ci-ó}
\hyphenation{e-qua-ci-ons}
\hyphenation{e-qui-va-lent-ment}
\hyphenation{es-criu-re}
\hyphenation{es-de-ve-ni-ment}
\hyphenation{es-de-ve-ni-ments}
\hyphenation{es-tàn-dard}
\hyphenation{e-xem-ple}
\hyphenation{e-xer-ci-ci}
\hyphenation{E-xer-ci-ci}
\hyphenation{ex-pe-ri-ment}
\hyphenation{ex-pe-ri-ments}
\hyphenation{ex-plí-ci-ta}
\hyphenation{fa-bri-quen}
\hyphenation{for-ma-ci-ó}
\hyphenation{im-plí-ci-ta}
\hyphenation{in-com-pa-ti-ble}
\hyphenation{in-de-pen-dent-ment}
\hyphenation{in-de-ter-mi-nat}
\hyphenation{in-dis-tin-ta-ment}
\hyphenation{ir-re-duc-ti-bles}
\hyphenation{lec-tu-ra}
\hyphenation{lí-ni-es}
\hyphenation{mos-tral}
\hyphenation{mul-ti-ce-re-al}
\hyphenation{mul-ti-pli-ca-da}
\hyphenation{mul-ti-pli-ca-ci-ons}
\hyphenation{mul-ti-pli-car}
\hyphenation{O-bli-ga-tò-ria}
\hyphenation{ob-ser-va-ci-ó}
\hyphenation{ob-te-nen}
\hyphenation{ob-te-nir}
\hyphenation{pa-ra-mè-tri-ques}
\hyphenation{pa-ra-me-trit-za-rem}
\hyphenation{pa-tei-xen}
\hyphenation{per-cen-tat-ge}
\hyphenation{per-pen-di-cu-lar}
\hyphenation{per-pen-di-cu-lars}
\hyphenation{po-li-no-mi}
\hyphenation{po-lí-ti-ca}
\hyphenation{pos-si-bi-li-tats}
\hyphenation{pos-si-bles}
\hyphenation{pre-ce-dits}
\hyphenation{prin-ci-pal}
\hyphenation{Pro-ba-bi-li-tat}
\hyphenation{pro-ba-bi-li-tats}
\hyphenation{pro-ce-di-ment}
\hyphenation{pro-ces-sa-ment}
\hyphenation{pro-duc-te}
\hyphenation{pro-por-cio-na}
\hyphenation{pro-por-cio-na-li-tat}
\hyphenation{pro-po-si-ci-ó}
\hyphenation{qua-dra-da}
\hyphenation{qual-se-vol}
\hyphenation{quals-se-vol}
\hyphenation{quan-ti-tat}
\hyphenation{Re-co-nei-xe-ment}
\hyphenation{Re-co-nei-xe-ment-Com-par-tir-I-gual}
\hyphenation{re-cor-dem}
\hyphenation{re-duc-ti-ble}
\hyphenation{re-em-pla-ça-ment}
\hyphenation{re-la-ci-o-na-des}
\hyphenation{re-pre-sen-tar}
\hyphenation{re-so-lu-ció}
\hyphenation{res-tant}
\hyphenation{Sa-bem}
\hyphenation{sa-tis-fac-to-ri}
\hyphenation{sa-tis-fan}
\hyphenation{seg-ment}
\hyphenation{se-güents}
\hyphenation{sis-te-ma}
\hyphenation{sub-con-junt}
\hyphenation{te-o-re-ma}
\hyphenation{trans-po-sa-da}
\hyphenation{tro-ba-rí-em}
\hyphenation{u-ni-ver-si-ta-ris}
\hyphenation{u-ni-ver-si-tat}
\hyphenation{vec-tors}
\hyphenation{ve-ri-fi-quen}
\hyphenation{vi-ce-ver-sa}
\end{hyphenrules}

% Start the document
\begin{document}

% Cover
\include{01-portada}

% Copyright
\newpage
\thispagestyle{empty}

\include{02-drets-d-autor}

% Table of Contents
\newpage
\tableofcontents

% Preface
\include{03-proleg}

% Àlgebra lineal
\include{04-algebra-lineal}

% Geometria
\include{05-geometria}

% Anàlisi
% PENDENT
% \include{06-analisi}

% Probabilitat
\include{07-probabilitat}

% Apèndixos
\appendix
\part{Apèndixs}
%% Repàs de conceptes previs
\include{08-apendix}

\include{11-examens}

\bookmarksetup{startatroot}% from https://stackoverflow.com/a/1484703
\backmatter

% Continguts aliens
\include{10-continguts-aliens}

% Bibliografia
\include{09-bibliografia}

%% list of figures
\listoffigures

%% list of tables
\listoftables

%% list of theorems
\listoftheorems[ignoreall,show={theorem,corollary,lemma,proposition,algorithm,claim,condition,definition,notation}]

% Glossari
\printindex

\end{document}

